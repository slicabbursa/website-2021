msgid ""
msgstr ""
"Project-Id-Version: French (Pepper & Carrot)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-29 16:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: French <http://localhost/projects/pepper-and-carrot/website/"
"fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.7.2\n"

#. # Global
msgctxt "$LANG->'TRANSLATED_BY'"
msgid ""
"Translation credit: <a href=\"https://www.davidrevoy.com\">David Revoy</a>"
msgstr "Traduction : <a href=\"https://www.davidrevoy.com\">David Revoy</a>"

#. ################################################################################
#. # Header HTML infos for search engine and title in tab:
msgctxt "$LANG->'PEPPERCARROT_TITLE'"
msgid "Pepper&amp;Carrot"
msgstr "Pepper&amp;Carrot"

#. // the title on top of the website
msgctxt "$LANG->'PEPPERCARROT_VEGETABLE'"
msgid "Pepper and Carrot"
msgstr "Poivron et Carotte"

#. // literal translation of the spice and the vegetable
#. // litteral translation of spices + vegetable
msgctxt "$LANG->'Website_DESCRIPTION'"
msgid ""
"Official homepage of Pepper&amp;Carrot, a free(libre) and open-source "
"webcomic about Pepper, a young witch and her cat, Carrot. They live in a "
"fantasy universe of potions, magic, and creatures."
msgstr ""
"Site officiel de Pepper&amp;Carrot, un webcomic libre et open-source au "
"sujet de Pepper, une jeune sorcière et son chat, Carrot. Ils vivent dans un "
"univers de potions, de magies et de créatures."

msgctxt "$LANG->'SUBTITLE'"
msgid "the Open Webcomic by David Revoy"
msgstr "le webcomic libre de David Revoy"

#. ################################################################################
#. # Top menu website:
msgctxt "$LANG->'HOMEPAGE'"
msgid "Homepage"
msgstr "Page d'acceuil"

msgctxt "$LANG->'WEBCOMICS'"
msgid "Webcomics"
msgstr "Webcomics"

msgctxt "$LANG->'BLOG'"
msgid "Blog"
msgstr "Blog"

msgctxt "$LANG->'PHILOSOPHY'"
msgid "Philosophy"
msgstr "Philosophie"

msgctxt "$LANG->'CONTRIBUTE'"
msgid "Contribute"
msgstr "Contribuer"

msgctxt "$LANG->'COMMUNITY'"
msgid "Community"
msgstr "Communauté"

msgctxt "$LANG->'WIKI'"
msgid "Wiki"
msgstr "Wiki"

msgctxt "$LANG->'SOURCES'"
msgid "Sources"
msgstr "Sources"

msgctxt "$LANG->'AUTHOR'"
msgid "Author"
msgstr "Auteur"

msgctxt "$LANG->'EXTRAS'"
msgid "Extras"
msgstr "Extras"

msgctxt "$LANG->'SHOP'"
msgid "Shop"
msgstr "Boutique"

msgctxt "$LANG->'FOLLOW'"
msgid "Follow Pepper&amp;Carrot on:"
msgstr "Suivre Pepper&amp;Carrot sur :"

#. # Top menu website buttons:
msgctxt "$LANG->'NAVIGATION_TRANSCRIPT'"
msgid "Transcript"
msgstr "Transcription"

msgctxt "$LANG->'NAVIGATION_TRANSCRIPT_ON'"
msgid "Click to show transcript"
msgstr "Afficher la transcription"

msgctxt "$LANG->'NAVIGATION_TRANSCRIPT_OFF'"
msgid "Click to hide transcript"
msgstr "Cacher la transcription"

msgctxt "$LANG->'NAVIGATION_TRANSCRIPT_UNAVAILABLE'"
msgid "Transcript not available for this episode and this language."
msgstr "Transcription indisponible pour cet épisode et cette langue."

msgctxt "$LANG->'NAVIGATION_HD'"
msgid "HD 2400px"
msgstr "HD 2400px"

msgctxt "$LANG->'NAVIGATION_HD_ON'"
msgid "Click to display high resolution images"
msgstr "Basculer vers la haute resolution"

msgctxt "$LANG->'NAVIGATION_HD_OFF'"
msgid "Click to display standard resolution images"
msgstr "Basculer vers la resolution standard"

msgctxt "$LANG->'NAVIGATION_DICTIONARY'"
msgid "Dictionary"
msgstr "Dictionnaire"

msgctxt "$LANG->'NAVIGATION_DICTIONARY_ALT'"
msgid "Click to open with Wordlink for fast dictionary search"
msgstr ""
"Cliquer pour ouvrir dans Wordlink pour une recherche rapide dans le "
"dictionnaire"

#. ################################################################################
#. # Top and bottom translation panel
msgctxt "$LANG->'ADD_TRANSLATION'"
msgid "add a translation"
msgstr "proposer une traduction"

msgctxt "$LANG->'CORRECTIONS'"
msgid "propose a correction"
msgstr "proposer une correction"

#. ################################################################################
#. # Page: Homepage
#. ################################################################################
#. # Page : Homepage
msgctxt "$LANG->'HOMEPAGE_BIG_TEXT'"
msgid ""
"\n"
"    A free(libre) and open-source webcomic<br/>\n"
"    supported directly by its patrons<br/>\n"
"    to change the comic book industry!<br/>\n"
msgstr ""
"\n"
"    Un webcomic libre et open-source<br/>\n"
"    financé directement par ses lecteurs<br/>\n"
"    pour changer l'industrie de la BD !<br/>\n"

msgctxt "$LANG->'HOMEPAGE_PATREON_INFO'"
msgid "For only $1 per new episode, become a patron today on Patreon"
msgstr ""
"Pour moins d' 1€ par nouvel épisode, devenez mécène de Pepper&amp;Carrot sur "
"Tipeee."

msgctxt "$LANG->'HOMEPAGE_MOREINFO_BUTTON'"
msgid "More information"
msgstr "Plus d'informations"

msgctxt "$LANG->'HOMEPAGE_PATREON_BUTTON'"
msgid "Become a patron of Pepper&amp;Carrot on Patreon"
msgstr "Devenez mécène de Pepper&amp;Carrot sur Tipeee"

msgctxt "$LANG->'PATRONAGE_BUTTON'"
msgid "Become a patron of Pepper&amp;Carrot"
msgstr "Devenez mécène de Pepper&amp;Carrot"

msgctxt "$LANG->'HOMEPAGE_LAST_EPISODE'"
msgid "Latest episode"
msgstr "Dernier épisode :"

msgctxt "$LANG->'HOMEPAGE_NEWS_UPDATE'"
msgid "News and updates"
msgstr "Nouvelles et mises à jour :"

msgctxt "$LANG->'HOMEPAGE_MOREPOSTS_BUTTON'"
msgid "More posts"
msgstr "Voir tout"

msgctxt "$LANG->'HOMEPAGE_MAINSERVICE_LINK'"
msgid "https://www.patreon.com/join/davidrevoy?"
msgstr "https://fr.tipeee.com/pepper-carrot"

msgctxt "$LANG->'HOMEPAGE_SUPPORTED_BY'"
msgid "supported by patrons."
msgstr "financé par ses lecteurs."

msgctxt "$LANG->'HOMEPAGE_ALTERNATIVES'"
msgid "Alternatives:"
msgstr "Alternatives:"

msgctxt "$LANG->'HOMEPAGE_FANART'"
msgid "Fan-art"
msgstr "Fan-art"

#. ################################################################################
#. # Page: Webcomics
#. ################################################################################
#. # Page : Webcomics
msgctxt "$LANG->'WEBCOMIC_EPISODE'"
msgid "Webcomic episodes"
msgstr "Épisodes"

msgctxt "$LANG->'WEBCOMIC_MAKINGOF'"
msgid "Making-of"
msgstr "Making-of"

msgctxt "$LANG->'WEBCOMIC_MAKINGOF_DESCRIPTION'"
msgid ""
"\n"
"<p>Making-of and <a href=\"http://www.davidrevoy.com/categorie5/tutorials\""
">art tutorials</a> are special bonuses sponsored by my <a href=\"https://www."
"patreon.com/join/davidrevoy?\">Patreons &#39;heroes&#39;</a></p>\n"
msgstr ""
"\n"
"    <p>Les Making-of et <a href=\"http://www.davidrevoy.com/categorie5/"
"tutorials\"> les tutoriels artistiques</a> sont des suppléments spéciaux "
"financés participativement grâce au soutien des mécènes sur <a href=\"https"
"://www.patreon.com/join/davidrevoy?\">Patreons</a></p>\n"

msgctxt "$LANG->'WEBCOMIC_ARTWORK'"
msgid "Artwork gallery"
msgstr "Galerie d'illustrations"

msgctxt "$LANG->'WEBCOMIC_SKETCHES'"
msgid "Sketches"
msgstr "Croquis"

msgctxt "$LANG->'WEBCOMIC_OLDER_COMICS'"
msgid "Older webcomics by the same author"
msgstr "Webcomics plus anciens, par le même auteur"

#. ################################################################################
#. # Page: Blog
#. # when contents are not English (no need to warn user the content is english only here):
#. #'LIMITATIONS' 	=> ' Content available in english only ',
#. ################################################################################
#. # Page : Blog
#. # An empty one in English (no need to warn user the content is english only here) :
msgctxt "$LANG->'LIMITATIONS'"
msgid ""
msgstr " Contenu disponible en anglais uniquement"

msgctxt "$LANG->'TRANSLATION_FALLBACK'"
msgid "(Translation is missing. Falling back to English)"
msgstr "(Traduction manquante. Redirection en anglais)"

#. ################################################################################
#. # Page: Philosophy
#. ################################################################################
#. # Page : Philosophy
msgctxt "$LANG->'PHILOSOPHY_TOP'"
msgid ""
"\n"
" <h2>Supported by patrons</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot project is only funded by its patrons, from all "
"around the world.\n"
"    Each patron sends a little money for each new episode published and gets "
"a credit at the end of the new episode.\n"
"    Thanks to this system, Pepper&amp;Carrot can stay independent and never "
"have to resort to advertising or any marketing pollution.</p>\n"
msgstr ""
"\n"
" <h2>Financé par ses lecteurs</h2>\n"
"\n"
"    <p>Le projet Pepper&amp;Carrot est financé par ses lecteurs venant du "
"monde entier.\n"
"    Tout lecteur peut envoyer une petite pièce pour mécéner la publication "
"d'un nouvel épisode.\n"
"    Il apparaît ensuite dans les remerciements de cet épisode. Grâce à ce "
"système, Pepper&amp;Carrot\n"
"    reste indépendant et protégé du monde éditorial, de la pub et du "
"marketing agressif.</p>\n"

msgctxt "$LANG->'DONATE_INTRO'"
msgid ""
"\n"
"    <p>\n"
"    It&#39;s easy to become the patron of Pepper&amp;Carrot for only $1 per "
"new episode <a href=\"https://www.patreon.com/join/davidrevoy?\">on "
"Patreon</a>.\n"
"    Patreon accepts credit cards from all around the world and also PayPal.\n"
"    </p>\n"
msgstr ""
"\n"
"    <p>\n"
"    Pour plus de commodité et moins de frais bancaires pour l'audience "
"francophone, j'ai mis en place <a href=\"https://www.tipeee.com/pepper-"
"carrot\" title=\"Pepper&amp;Carrot on tipeee\">une page sur Tipeee</a>.\n"
"    Il est également possible <a href=\"https://www.patreon.com/join/"
"davidrevoy?\">de soutenir le projet via Patreon</a> (international).\n"
"    Patreon et Tipeee acceptent les Cartes Bleues, Visa et Paypal, etc.\n"
"    </p>\n"

msgctxt "$LANG->'PHILOSOPHY_BOTTOM'"
msgid ""
"\n"
"    <img alt=\"illustration representing patronage\" src=\"0_sources/0ther/"
"misc/low-res/2015-02-09_philosophy_01-support_by-David-Revoy.jpg\">\n"
"\n"
"    <h2>100% free(libre), forever, no paywall</h2>\n"
"\n"
"    <p>All the content I produce about Pepper&amp;Carrot is on this website, "
"free(libre) and available to everyone.\n"
"    I respect all of you equally: with or without money. All special bonuses "
"I make for my patrons are also posted here.\n"
"    Pepper&amp;Carrot will never ask you to pay anything or to get a "
"subscription to get access to new content.</p>\n"
"\n"
"        <img alt=\"Illustration representing paywall\" src=\"0_sources/0ther/"
"misc/low-res/2015-02-09_philosophy_03-paywall_by-David-Revoy.jpg\">\n"
"\n"
"    <h2>Open-source and permissive</h2>\n"
"\n"
"    <p>I want to give people the right to share, use, build and even make "
"money upon the work I&#39;ve created.\n"
"    All pages, artworks and content were made with Free(Libre) Open-Sources "
"Software on GNU/Linux, and all sources are on this website, &#39;Source&#39; "
"menu.\n"
"    Commercial usage, translations, fan-art, prints, movies, video-games, "
"sharing, and reposts are encouraged.\n"
"    You just need to give appropriate credit to the authors (artists, "
"correctors, translators involved in the artwork you want to use), provide a "
"link to the license, and indicate if changes were made. You may do so in any "
"reasonable manner, but not in any way that suggests the authors endorse you "
"or your use.</p>\n"
"\n"
"    <div class=\"button\">\n"
"        <a href=\"https://creativecommons.org/licenses/by/4.0/\" title=\"For "
"more information, read the Creative Commons Attribution 4.0\">\n"
"            License: Creative Commons Attribution 4.0\n"
"        </a>\n"
"    </div>\n"
"\n"
"        <img alt=\"Illustration representing open-source media\" src="
"\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_04-open-source_by-David-"
"Revoy.jpg\">\n"
"\n"
"    <h2>Quality entertainment for everyone, everywhere</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot is a comedy/humor webcomic suited for everyone, "
"every age.\n"
"    No mature content, no violence. Free(libre) and open-source, Pepper&amp;"
"Carrot is a proud example of how cool free-culture can be.\n"
"    I focus a lot on quality, because free(libre) and open-source doesn&#39;"
"t mean bad or amateur. Quite the contrary.</p>\n"
"\n"
"        <img alt=\"Illustration representing comic pages flying around the "
"world\" src=\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_05"
"-everyone_by-David-Revoy.jpg\">\n"
"\n"
"    <h2>Let&#39;s change comic industry!</h2>\n"
"\n"
"    <p>\n"
"      Without intermediary between artist and audience you pay less and I "
"benefit more. You support me directly.\n"
"      No publisher, distributor, marketing team or fashion police can force "
"me to change Pepper&amp;Carrot to fit their vision of &#39;the market&#39;.\n"
"      Why couldn&#39;t a single success &#39;snowball&#39; to a whole "
"industry in crisis? We&#39;ll see…\n"
"    </p>\n"
"\n"
"    <div class=\"button\">\n"
"      <a href=\"?static12/donate\" title=\"For only $1 per new episode, "
"become a patreon of Pepper&amp;Carrot\">\n"
"        Help me boost Pepper&amp;Carrot production today.\n"
"      </a>\n"
"    </div>\n"
"\n"
"    <img alt=\"Illustration: comic book industry vs. Patreon support\" src="
"\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_06-industry-change_by-"
"David-Revoy.jpg\">\n"
" "
msgstr ""
"\n"
"    <img alt=\"Illustration représentant le mécénat moderne\" src=\"0_sources"
"/0ther/misc/low-res/2015-02-09_philosophy_01-support_by-David-Revoy.jpg\">\n"
"\n"
"    <h2>100% libre, pour toujours</h2>\n"
"\n"
"    <p>Tous les contenus que je crée autour et pour Pepper&amp;Carrot sont "
"sur ce site, libres et disponibles pour tous.\n"
"    Je vous respecte tous au même niveau : avec ou sans argent. Tous les "
"bonus que je fais pour mes mécènes sont ici également.\n"
"    Pepper&amp;Carrot ne vous demandera jamais de payer quoi que ce soit ou "
"de s'inscrire quelque part pour avoir accès au contenu.</p>\n"
"\n"
"        <img alt=\"Illustration représentant les péages du net\" src="
"\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_03-paywall_by-David-"
"Revoy.jpg\">\n"
"\n"
"    <h2>Open-source et très permissif</h2>\n"
"\n"
"    <p>Je veux donner aux autres le droit d'utiliser, de changer et même de "
"commercialiser leurs projets utilisant Pepper&amp;Carrot.\n"
"    Toutes pages, dessins et autres contenus ont été fabriqués avec des "
"logiciels libres (Gnu/Linux), et toutes les sources sont sur ce site (menu  "
"'Sources').\n"
"    L'utilisation commerciale, les traductions, les fan-arts, l'impression, "
"les films, les jeux-vidéo et le re-partage sur le net sont encouragés.\n"
"    Je vous demande seulement de créditer les œuvres de leur(s) auteur(s) "
"respectif(s) (artistes, correcteurs, traducteurs investi(e)s dans la "
"création de l'œuvre), d'intégrer un lien vers la licence et indiquer si des "
"modifications ont été effectuées à l'œuvre. Vous devez indiquer ces "
"informations par tous les moyens raisonnables, sans toutefois suggérer que "
"les auteur(s) vous soutiennent ou soutiennent la façon dont vous avez "
"utilisé leurs œuvres.</p>\n"
"\n"
"    <div class=\"button\">\n"
"        <a href=\"https://creativecommons.org/licenses/by/4.0/deed.fr\" "
"title=\"Plus d'information en français sur Creative Commons Attribution 4.0\""
">\n"
"            Licence : Creative Commons Attribution 4.0\n"
"        </a>\n"
"    </div>\n"
"\n"
"        <img alt=\"Illustration représentant les médias open-sources\" src="
"\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_04-open-source_by-David-"
"Revoy.jpg\">\n"
"\n"
"    <h2>Un divertissement de qualité, partout et pour tous</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot est un webcomic (une BD sur Internet) humoristique "
"convenant pour tous les âges.\n"
"    Aucune scène de violence, ni de sexualité ne sera publiée ici. Libre et "
"open-source, Pepper&amp;Carrot\n"
"    se veut être un exemple responsable de médias libres. L' accent est mis "
"sur la qualité, l'éthique et le\n"
"    professionnalisme, parce que \"libre et open-source\" ne signifie pas "
"\"au rabais ou à l'arrache\". Au contraire.</p>\n"
"\n"
"        <img alt=\"Illustration pour tous\" src=\"0_sources/0ther/misc/"
"low-res/2015-02-09_philosophy_05-everyone_by-David-Revoy.jpg\">\n"
"\n"
"    <h2>Changeons l'industrie de la BD !</h2>\n"
"\n"
"    <p>\n"
"        Avec moins d'intermédiaires entre lecteurs et auteurs, tout le monde "
"y gagne ; vous me financez directement.\n"
"        Aucun éditeur, distributeur ou commercial ne peut exercer des "
"changements sur Pepper&amp;Carrot afin de le faire rentrer dans une case du "
"'marché'.\n"
"        ...Et pourquoi Pepper&amp;Carrot ne pourrait-il pas amorcer un "
"changement et ainsi inspirer une industrie en crise ? Essayons !\n"
"    </p>\n"
"\n"
"    <div class=\"button\">\n"
"      <a href=\"?static12/donate\" title=\"Pour seulement 1€ par nouvel "
"épisode, soutenez Pepper&amp;Carrot\">\n"
"        Aidez moi à booster la production de Pepper&amp;Carrot dès "
"aujourd'hui.\n"
"      </a>\n"
"    </div>\n"
"\n"
"        <img alt=\"Illustration: l'industrie de la BD VS financement par "
"mécénat\" src=\"0_sources/0ther/misc/low-res/2015-02-09_philosophy_06"
"-industry-change_by-David-Revoy.jpg\">\n"
"\n"
"\n"
" "

#. ################################################################################
#. # Page: Contribute
#. ################################################################################
#. # Page : Contribute
msgctxt "$LANG->'CONTRIBUTE_TITLE'"
msgid "Contribute"
msgstr "Contribuer"

msgctxt "$LANG->'CONTRIBUTE_TOP'"
msgid ""
"\n"
"    <p>Thanks to the <a href=\"?static6/sources\" title=\"Sources page\""
">open sources</a> and the <a href=\"http://creativecommons.org/licenses/by/4."
"0/\">\n"
"    creative commons license</a> you can contribute to Pepper&amp;Carrot in "
"many ways:</p>\n"
msgstr ""
"\n"
"    <p>Grâce au partage des <a href=\"?static6/sources\" title=\"Sources "
"page\">sources</a> et à sa <a href=\"http://creativecommons.org/licenses/by/4"
".0/\">\n"
"    licence Creative Commons Attribution</a>, vous pouvez contribuer à "
"Pepper&amp;Carrot de multiples manières :</p>\n"

msgctxt "$LANG->'CONTRIBUTE_FANART'"
msgid ""
"\n"
"    <h2>Fan-art</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot is open to fan-art: drawings, scenarios, "
"sculptures, 3D models, fan-fiction. Send them to me (<a href=\"mailto"
":info@davidrevoy.com\">info@davidrevoy.com</a>, or poke me on social "
"networks) to appear in the fan-art gallery:</p>\n"
msgstr ""
"\n"
"    <h2>Fan-art</h2>\n"
"    <p>Pepper&amp;Carrot est ouvert aux fan-arts : dessins, scénarios, "
"sculptures, 3D, fan-fictions... et vous pouvez me les envoyer car j'adore ça "
":\n"
"    <br/> (<a href=\"mailto:info@davidrevoy.com\">info@davidrevoy.com</a> , "
"ou notifiez-en moi sur un réseau social).</p>\n"

msgctxt "$LANG->'CONTRIBUTE_DERIVATIONS'"
msgid ""
"\n"
"    <h2>Derivatives</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot can be adapted to many projects or products, why "
"not do your own or join an existing one?</p>\n"
msgstr ""
"\n"
"    <h2>Adaptations</h2>\n"
"    <p>Pepper&amp;Carrot peut-être décliné en beaucoup de produits ou de "
"projets (même commerciaux), et pourquoi pas le vôtre ?<br/>\n"
"    Voici une galerie de projets existants autour de l'univers de la série.\n"
"    </p>\n"

msgctxt "$LANG->'CONTRIBUTE_TRANSLATION'"
msgid ""
"\n"
"    <h2>Translations and corrections</h2>\n"
"\n"
"    <p>Pepper&amp;Carrot website is designed to be multilingual and accept "
"any language (including extinct ones, or fictive ones). The sources of this "
"page are at your disposal for you to translate them. Check the <a href=\"?"
"static14/documentation&page=010_Translate_the_comic\">reference tutorial</a> "
"for more information on how to add your translation.</p>\n"
msgstr ""
"\n"
"    <h2>Traductions et corrections</h2>\n"
"\n"
"    <p>Le site internet de Pepper&amp;Carrot est conçu pour accepter toutes "
"les langues (même fictives et éteintes). Les sources de cette page sont à "
"votre disposition pour la correction et la traduction. Visitez le <a href=\"?"
"static14/documentation&page=010_Translate_the_comic\">tutoriel</a> pour plus "
"d'informations sur l'ajout d'une nouvelle traduction ou la proposition d'une "
"amélioration.</p>\n"

msgctxt "$LANG->'CONTRIBUTE_SOCIAL'"
msgid "Social networks"
msgstr "Réseaux sociaux:"

msgctxt "$LANG->'CONTRIBUTE_PRESS'"
msgid ""
"\n"
"    <h2>Press</h2>\n"
"\n"
"    <p>Be the publisher of Pepper&amp;Carrot! Write articles, create posts "
"on websites, share and build a community on your favorite social networks. "
"You can download the <a href=\"?static6/sources\" title=\"Sources page\""
">press kit</a> on the \"sources\" page.</p>\n"
msgstr ""
"\n"
"    <h2>Presse:</h2>\n"
"\n"
"    <p>Devenez l'éditeur de Pepper&amp;Carrot, écrivez des articles, "
"partagez les pages, et construisez votre propre communauté. Des fichiers "
"archives au format zip sont à votre disposition dans les Sources avec logos "
"et dessins en haute-définition. Ils sont classés sous la catégorie <a href=\""
"?static6/sources\" title=\"Sources page\">Press Kit</a>.</p>\n"

msgctxt "$LANG->'CONTRIBUTE_OTHER'"
msgid ""
"\n"
"    <h2>Other ideas to contribute…</h2>\n"
"\n"
"    <p>Everyone can contribute in different ways:<br/>\n"
"        <b>Developers:</b> Create an application to read Pepper&amp;Carrot "
"on mobile device.<br/>\n"
"        <b>Musician:</b> Create music themes for Pepper&amp;Carrot.<br/>\n"
"        <b>Writer:</b> Propose new Pepper&amp;Carrot scenarios.<br/>\n"
"        <b>Journalist:</b> Talk about Pepper&amp;Carrot in traditional media "
"(printed press, tv, etc.)<br/>\n"
"        <b>Printer:</b> Print posters or goodies with Pepper&amp;Carrot on "
"it.<br/>\n"
"    </p>\n"
msgstr ""
"\n"
"    <h2>Autres idées de participations</h2>\n"
"\n"
"    <p>Tout le monde peut apporter sa pierre à l'édifice : <br/>\n"
"        <b>Développeurs :</b> Création d'applications utilisant les "
"graphismes de Pepper&amp;Carrot.<br />\n"
"        <b>Musiciens :</b> Création de thèmes musicaux pour la série "
"Pepper&amp;Carrot.<br />\n"
"        <b>Écrivains : </b> Proposition de roman, ou nouveau scénario pour "
"Pepper&amp;Carrot.<br />\n"
"        <b>Journalistes: </b> Informations à propos du projet Pepper&amp;"
"Carrot dans les médias traditionnels (presse écrite, télévision, etc.)<br />"
"\n"
"        <b>Imprimeurs :</b> Impression de posters ou de petits objets sur le "
"thème de Pepper&amp;Carrot.<br />\n"
"    </p>\n"

msgctxt "$LANG->'CONTRIBUTE_IRC'"
msgid ""
"\n"
"    <h2 id=\"irc\">IRC channel</h2>\n"
"\n"
"    <p>Chat and discuss about Pepper&amp;Carrot. I&#39;m around at French "
"day time (nickname: deevad)<br/>\n"
msgstr ""
"\n"
"    <h2 id=\"irc\">Canal de discussion</h2>\n"
"\n"
"    <p>Un canal de discussion IRC (* Internet Chat Relay) existe en anglais "
"sur le serveur Freenode, nommé #pepper&carrot. Ce canal est à votre "
"disposition pour discuter au sujet de Pepper&amp;Carrot. J'y suis connecté "
"fréquement (pseudo: deevad). Le lien ci-dessous fonctionnera dans votre "
"navigateur internet sans besoin d'installer quoi que ce soit.<br/>\n"

#. ################################################################################
#. # Page: Sources
#. ################################################################################
#. # Page : Sources
msgctxt "$LANG->'SOURCES_TITLE'"
msgid "Sources"
msgstr "Sources"

msgctxt "$LANG->'SOURCES_TOP'"
msgid ""
"\n"
"    <p><b>Welcome to the sources download center!</b><br/>\n"
"    Here you&#39;ll find the source files of Pepper&amp;Carrot and more.<br/>"
"\n"
"    All digital painting files here are compatible with the latest version "
"of <a href=\"https://krita.org/\">Krita</a>.</p>\n"
msgstr ""
"\n"
"    <p><b>Bienvenue au centre de téléchargement des fichiers sources !</b><"
"br/>\n"
"    Vous trouverez ici tous les fichiers sources de Pepper&amp;Carrot.<br/>\n"
"    Les fichiers de peinture numérique sont compatibles avec la dernière "
"version de <a href=\"https://krita.org/\">Krita</a></p>\n"

msgctxt "$LANG->'SOURCES_BOTTOM'"
msgid ""
"\n"
"    <p>By downloading and working with those files you agree to respect the "
"terms of the <br/>\n"
"    <a href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons "
"Attribution license</a>.\n"
"    Check the README files in each project for more infos.</p>\n"
"\n"
"    <p><b>Code repositories for translations, scripts, the website, "
"graphics, etc.:</b><br/></p>\n"
msgstr ""
"\n"
"    <p>Par le téléchargement de ces fichiers, vous certifiez être en accord "
"avec les termes <br/>\n"
"     de <a href=\"http://creativecommons.org/licenses/by/4.0/deed.fr\">la "
"licence Creative Commons Attribution</a>.\n"
"    Consultez le fichier README<br/>\n"
"    de chaque projet pour plus d'infos.</p>\n"
"\n"
"    <p><b>Dépôts de codes pour traductions, graphisme, site, script :</b><br/"
"></p>\n"

msgctxt "$LANG->'SOURCE_COVER'"
msgid "Cover images"
msgstr "Couvertures"

msgctxt "$LANG->'SOURCE_KRITA'"
msgid "Krita source files"
msgstr "Fichiers Krita sources"

msgctxt "$LANG->'SOURCE_TRANSLATOR'"
msgid "Translator's pack"
msgstr "Pack à l'usage des traducteurs"

msgctxt "$LANG->'SOURCE_WEB'"
msgid "Web: <span class=\"sourceinfos\">90ppi, lightweight jpg</span>"
msgstr "Internet <span class=\"sourceinfos\"> 90ppi, fichier jpg léger</span>"

msgctxt "$LANG->'SOURCE_PRINT'"
msgid "Print: <span class=\"sourceinfos\">300ppi, lossless quality</span>"
msgstr ""
"Impression <span class=\"sourceinfos\"> 300ppi, sans perte de qualité </span>"

msgctxt "$LANG->'SOURCE_MONTAGE'"
msgid "Montage: <span class=\"sourceinfos\">all panels in a single page</span>"
msgstr ""
"Collage <span class=\"sourceinfos\"> Toutes les cases dans une seule image "
"</span>"

#. ################################################################################
#. # Page: Author
#. ################################################################################
#. # Page : Author
msgctxt "$LANG->'AUTHOR_TITLE'"
msgid "About David Revoy"
msgstr "À propos de David Revoy"

msgctxt "$LANG->'AUTHOR_BIO'"
msgid ""
"\n"
"    <p>Hi, my name is David Revoy and I&#39;m a French artist born in 1981. "
"I&#39;m self-taught and passionate about drawing, painting, cats, computers, "
"Gnu/Linux open-source culture, Internet, old school RPG video-games, old "
"mangas and anime, traditional art, Japanese culture, fantasy…<br/>\n"
"    <br/>\n"
"    After more than 10 years of freelance in digital painting, teaching, "
"concept-art, illustrating and art-direction I decided to start my own "
"project. I finally found a way to mix all my passions together, the result "
"is Pepper&amp;Carrot.</p>\n"
"\n"
"    <p>My portfolio:</p>\n"
msgstr ""
"\n"
"    <p>Bonjour, je m'appelle David Revoy et je suis un artiste français né à "
"Reims en 1981 et habitant à Montauban. Je suis autodidacte et passionné sur "
"beaucoup de sujets : le dessin, la peinture, les chats, l'informatique, l"
"'open-source, Gnu/Linux, Internet, les vieux jeux-vidéo, les vieux mangas, "
"l'art, la culture japonaise et la fantasy...<br />\n"
"    <br />\n"
"    Après plus de 10 ans de travail en indépendant à faire des commandes "
"pour l'édition, l'enseignement, les studios de jeux-vidéo ou les films "
"d'animation, j'ai trouvé ma voie dans un projet personnel qui mixe toutes "
"mes passions ; et ce projet, c'est Pepper&amp;Carrot.</p>\n"
"\n"
"    <p>Mon portfolio :</p>\n"

msgctxt "$LANG->'AUTHOR_TODO_DREAM'"
msgid ""
"\n"
"    <h3 id=\"dream\">Dream TO-DO list</h3>\n"
"\n"
"    <p>\n"
"    ☐ Give a talk in a Japanese comic convention about Pepper&amp;Carrot.<br/"
">\n"
"    ☐ Play with a gamepad to a Pepper&amp;Carrot video-game.<br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/en/article457/dream-to-do-"
"list-100-fan-arts\"> Get a gallery of 100 fan-arts.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/en/article376/dream-to-do-list-a"
"-wikipedia-page\">Get a Wikipedia page.</a><br/>\n"
"    ☑ <a href=\"https://www.davidrevoy.com/tag/cosplay\">Receive a photo of "
"a Pepper cosplay.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/article304/dream-to-do-list-a"
"-cat-named-carrot\">Receive a photo of a red cat named Carrot.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/article302/peppercarrot-has-"
"over-500-supporters\">Be supported by 500 patreons.</a><br/>\n"
"    ☐ Reach episode 100!<br/>\n"
"    </p>\n"
msgstr ""
"\n"
"    <h3 id=\"dream\">Mes rêves pour ce projet :</h3>\n"
"\n"
"    <p>\n"
"    ☐ Donner une conférence au Japon dans une convention Manga à propos de "
"Pepper&amp;Carrot.<br/>\n"
"    ☐ Jouer avec une manette de jeu à un jeu vidéo dérivé de "
"Pepper&amp;Carrot.<br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/en/article457/dream-to-do-"
"list-100-fan-arts\"> Avoir 100 fan-arts dans la galerie du site.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/en/article376/dream-to-do-list-a"
"-wikipedia-page\">Avoir une page sur Wikipedia.</a><br/>\n"
"    ☑ <a href=\"https://www.davidrevoy.com/tag/cosplay\">Recevoir une photo "
"d'un cosplay de Pepper.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/article304/dream-to-do-list-a"
"-cat-named-carrot\">Recevoir une photo d'un chat roux nommé Carrot en "
"hommage à Carrot.</a><br/>\n"
"    ☑ <a href=\"http://www.peppercarrot.com/article302/peppercarrot-has-"
"over-500-supporters\">Être soutenu par 500 mécènes via Patreon.</a><br/>\n"
"    ☐ Atteindre l'épisode 100 !<br/>\n"
"    </p>\n"

msgctxt "$LANG->'AUTHOR_CARREER_TITLE'"
msgid "My career in 7 bubbles"
msgstr "Mon parcours en 7 bulles :"

msgctxt "$LANG->'AUTHOR_CARREER_BUBBLE_DESCRIPTIONS'"
msgid "<p></p>"
msgstr "<p>(Schéma en anglais, non traduit)</p>"

#. ################################################################################
#. # Website General: Footer
#. ################################################################################
#. # Website General : Footer
msgctxt "$LANG->'FOOTER_CONTENT'"
msgid ""
"\n"
"    <p>Webcomic, artworks and texts are licensed under a <a href=\"https"
"://creativecommons.org/licenses/by/4.0/\" title=\"For more information, read "
"the Creative Commons Attribution 4.0\">Creative Commons Attribution 4.0</a> "
"license, unless otherwise noted in the page.<br/>\n"
"    Attribution to \"David Revoy, www.davidrevoy.com\". Contact me: <a href="
"\"mailto:info@davidrevoy.com\">info@davidrevoy.com</a> for more "
"information.</p>\n"
"\n"
"    <p>Website powered by <a href=\"http://www.pluxml.org\" title=\"PluXml\""
">PluXml</a></p>\n"
msgstr ""
"\n"
"    <p>Le Webcomic, les dessins et textes sont distribués sous licence <a "
"href=\"https://creativecommons.org/licenses/by/4.0/deed.fr\" title=\"Plus "
"d'informations en français sur Creative Commons Attribution 4.0\">Creative "
"Commons Attribution 4.0</a>, à moins qu'autre chose ne soit mentionné dans "
"la page.<br/>\n"
"    Auteur: David Revoy, www.davidrevoy.com. Contactez-moi, <a href=\"mailto"
":info@davidrevoy.com\">info@davidrevoy.com</a> , pour toute demande de "
"renseignements.</p>\n"
"\n"
"    <p>Site web utilisant <a href=\"http://www.pluxml.org\" title=\"PlXml\""
">PluXml</a></p>\n"

#. ################################################################################
#. # Eshop
msgctxt "$LANG->'ESHOP_COMIC'"
msgid "Comic books and artbooks are on:"
msgstr "BDs et recueil d’illustrations sont sur:"

msgctxt "$LANG->'ESHOP_SHOP'"
msgid "Prints, mugs, tee-shirts and more on:"
msgstr "Posters, mugs, tee-shirts et plus sur:"

#. ################################################################################
#. # various utils:
#. ################################################################################
#. # various utils :
msgctxt "$LANG->'UTIL_NEXT_EPISODE'"
msgid "next episode"
msgstr "suivant"

msgctxt "$LANG->'UTIL_PREVIOUS_EPISODE'"
msgid "prev episode"
msgstr "précédent"

msgctxt "$LANG->'UTIL_EPISODE'"
msgid "episode"
msgstr "épisode"

msgctxt "$LANG->'UTIL_BACK_TO_GALLERY'"
msgid "back to gallery"
msgstr "retour à la galerie"

msgctxt "$LANG->'UTIL_MORE'"
msgid "more"
msgstr "plus"

msgctxt "$LANG->'UTIL_PAGE'"
msgid "page"
msgstr "page"

msgctxt "$LANG->'UTIL_BY'"
msgid "by"
msgstr "par"

msgctxt "$LANG->'UTIL_LICENSE'"
msgid "license"
msgstr "licence"

msgctxt "$LANG->'UTIL_DOT'"
msgid "."
msgstr "."

msgctxt "$LANG->'FIRST'"
msgid "first"
msgstr "premier"

msgctxt "$LANG->'LAST'"
msgid "last"
msgstr "dernier"
