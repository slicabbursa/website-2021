<?php if ($root=="") exit;

echo '<div class="container">'."\n";
echo '  <main class="main grid" role="main">'."\n";
echo '    <section class="col sml-12 med-12 lrg-12 sml-text-center">'."\n";

echo '<br/>';
echo '<br/>';
# (user visit here because of the 'save button', in mod-header.php)
$previous_link = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) {
    $previous_link = $_SERVER['HTTP_REFERER'];
}
echo '<a class="translabutton" href="'.$previous_link.'">&lt; '._("Return to the previous page").'</a>';

echo '<h2>'._("Language saved as your favorite!").'</h2><br/>'."\n";

echo '<br/><br/><img src="'.$root.'/'.$sources.'/0ther/misc/low-res/2015-03-17_happy-dance_by-David-Revoy.jpg" /><br/>'."\n";

echo '    </section>'."\n";
echo '  </main>'."\n";
echo '</div>'."\n";
?>
