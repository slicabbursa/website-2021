<?php if ($root=="") exit;


# Include the language selection menu and credit engine
include($file_root.'core/mod-menu-lang.php');
include($file_root.'core/lib-credits.php');

echo '  <div style="clear:both"></div>'."\n";
echo '<div class="container" style="max-width: 1160px;">'."\n";
echo '  <section class="support col sml-12 med-12 lrg-10"'."\n";
echo '>'."\n";




echo '<h3 style="font-size: 1.8rem">'._("Support Pepper&Carrot on:").'</h3>'."\n";

echo '   <a class="buttonpage" style="font-size: 1.2rem; padding: 1.5rem 4rem; background-color:#F86754; margin: 1.5rem 3rem 3rem 3rem;" href="https://www.patreon.com/join/davidrevoy?"> '."\n";
echo '     '._("Patreon").' '."\n";
echo '   </a> '."\n";

echo '<br/>'._("Other possibilities:").'<br/>'."\n";

echo '<a class="buttonpage" style="font-size: 1.2rem; padding: 0.7rem 2.2rem 0.7rem 1.2rem; background-color:#F6C915; margin-right: 0.6rem;" title="&rarr; Liberapay" href="https://liberapay.com/davidrevoy/">Liberapay</a>'."\n";
echo '<a class="buttonpage" style="font-size: 1.2rem; padding: 0.7rem 2.2rem 0.7rem 1.2rem; background-color:#D84759; margin-right: 0.6rem;" href="https://fr.tipeee.com/pepper-carrot" target="blank">Tipeee</a>'."\n";
echo '<a class="buttonpage" style="font-size: 1.2rem; padding: 0.7rem 2.2rem 0.7rem 1.2rem; background-color:#0070BA; margin-right: 0.6rem;" href="https://paypal.me/davidrevoy" target="blank">Paypal</a>'."\n";
echo '</p>'."\n";

echo '<p>'._("Wire transfer:").'<br/>'."\n";
echo '<strong>IBAN:</strong> FR6230002040630000090557K69<br />'."\n";
echo '<strong>RIB:</strong> 30002040630000090557K69<br/>'."\n";
echo '<strong>BIC:</strong> CRLYFRPP<br />'."\n";
echo '</p>'."\n";

echo '<p>'._("Address for gifts:").'<br/>'."\n";
echo ''."\n";
echo '  Cabinet Dentaire (pour David REVOY)<br />135 Avenue des Mourets<br/>'."\n";
echo '  82000 MONTAUBAN - FRANCE<br/>'."\n";
echo '</p>'."\n";




echo '  </section>'."\n";
echo ''."\n";
echo '  <div style="clear:both"></div>'."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo '</div>'."\n";
echo ''."\n";

?>
